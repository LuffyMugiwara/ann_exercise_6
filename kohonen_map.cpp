/* Programming Exercise 6
 *
 * 108012209500 Benjamin Lentz
 * 108012234853 Emanuel Durmaz
 *
 */

#include "kohonen_map.h"

#include <cassert>
#include <cmath>

/// Creates a Kohonen map of size RxS and sets the learning rate to eta.
ann::kohonen_map::kohonen_map (size_t R_a, size_t S_a, real_t eta_a)
: // @task: Initialize member variables.
  // @hint: Adapt functions R and S (in kohonen_map.h), if applicable.
  mues0_m(R_a,S_a),
  mues1_m(R_a,S_a),
  mues2_m(R_a,S_a) //all values stay zero
{
    eta_m = eta_a;

    real_t min = -2.0;
    real_t max = 2.0;

    //initialize
    for(size_t r = 0; r < R(); r++)
    {
        for(size_t s = 0; s < S(); s++)
        {
            mues0_m(r,s) = ((max-min) /(R()-1.0)) * r + min;
            mues1_m(r,s) = ((max-min) /(S()-1.0)) * s + min;
            //mues2_m(r,s) = 0;
        }
    }

}

/// Presents a sample to this Kohonen map and moves the neurons accordingly.
void ann::kohonen_map::adapt_to (real_t x0_a, real_t x1_a, real_t x2_a)
{
  // @task: Find (indices of) the best matching neuron.

    real_t dist_smallest = std::sqrt(square(mues0_m(0,0) -  x0_a) + square(mues1_m(0,0) - x1_a) + square(mues2_m(0,0) - x2_a)); //we need a start value for the search algo
    size_t r_smallest = 0;
    size_t s_smallest = 0;

    //search for best match
    for(size_t r=0; r < R(); r++)
    {
        for(size_t s=0; s < S(); s++)
        {
            //euclidean distance between a prototype at (r,s) and given x-vector (meant are the values inside the vectors)
            real_t dist = std::sqrt(square(mues0_m(r,s) -  x0_a) + square(mues1_m(r,s) - x1_a) + square(mues2_m(r,s) - x2_a));

            if(dist_smallest > dist) //check if smaller
            {
                //save current state
                dist_smallest = dist;
                r_smallest = r;
                s_smallest = s;
            }
        }
    }


  // @task: Move all neurons according to the Kohonen learning rule.
    for(size_t r=0; r < R(); r++)
    {
        for (size_t s = 0; s < S(); s++)
        {
            real_t fN = f_N(r,s,r_smallest,s_smallest); //neighborhood function
            mues0_m(r,s) = mues0_m(r,s) + eta_m * fN * (x0_a - mues0_m(r,s));
            mues1_m(r,s) = mues1_m(r,s) + eta_m * fN * (x1_a - mues1_m(r,s));
            mues2_m(r,s) = mues2_m(r,s) + eta_m * fN * (x2_a - mues2_m(r,s));
        }
    }

}

/// Gives access (read/write) to an element of a neuron's prototype.
/// The first two indices specify the neuron and the third the chooses the
/// element of its prototype.
ann::kohonen_map::real_t&
ann::kohonen_map
::
operator() (size_t r_a, size_t s_a, size_t mue_index_a)
{
  assert(r_a < R() && s_a < S() && mue_index_a < mue_size());
  if (mue_index_a == 0)  return mues0_m(r_a, s_a);
  else if (mue_index_a == 1)  return mues1_m(r_a, s_a);
  // else
  return mues2_m(r_a, s_a);
}

/// Gives access (read only) to an element of a neuron's prototype.
/// The first two indices specify the neuron and the third the chooses the
/// element of its prototype.
const ann::kohonen_map::real_t&
ann::kohonen_map
::
operator() (size_t r_a, size_t s_a, size_t mue_index_a) const
{
  assert(r_a < R() && s_a < S() && mue_index_a < mue_size());
  if (mue_index_a == 0)  return mues0_m(r_a, s_a);
  else if (mue_index_a == 1)  return mues1_m(r_a, s_a);
  // else
  return mues2_m(r_a, s_a);
}


/// Returns the value of the neighborhood function f_N for (the two neurons
/// at) the given indices.
ann::kohonen_map::real_t ann::kohonen_map::f_N (size_t r0_a, size_t s0_a, size_t r1_a, size_t s1_a) const
{
  // @task: Implement the neighborhood function.
  // @hint: This depends solely on the indices, the prototypes are not used.

    real_t div = 2.0;
    //div = 10.0;

    // r' and s' (r1_a and s1_a) are best matching
    return std::exp(-1.0 * (square((int)r0_a - (int)r1_a) + square((int)s0_a - (int)s1_a))/div ); //need some int-casts, because sometimes we get negative values from the subtraction
}

