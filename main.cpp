/* Programming Exercise 6
   Work is to be done in main.cpp and kohonen_map.cpp.
   Please find the usual  @task  markers and follow their instructions.
   There are also at least one  @hint.
   There are no markers to remind of questions to be answered.
   Please feel free to fully adapt all code to your likings.
   Sticking close to the templates is recommended, though.

   Please leave your names here.
   author(s):

   108012209500 Benjamin Lentz
   108012234853 Emanuel Durmaz

    f)
    Die anfängliche gleichmäßige Verteilung von mues0_m und mues1_m ändert sich durch den Lernprozess nur geringfügig.
    Änderung traten vor allem bei mues2_m stark auf. Durch die stellenweise sehr hohe Steigung von mues2_m wurden die Zentren auseinanderbewegt,
    was an diesen Stellen zu einer geringen Dichte führte. Dies tritt vor allem an den Rändern des Torus auf.
    Durch mehrfachte Iterationen zeichnet sich die Form des Torus genauer ab,
    was wiederum zu einer höheren Steigung von mues2_m an den Rändern führt und somit zu einer noch geringeren Dichte.

    Wird im Exponenten die 2 im Nenner durch eine 10 ersetzt, bewegen die Zentren sich stärker durch die Eingangsvektoren x.
    Dadurch wird die darzustellende Form schneller erkennbar. Allerdings prägt sich die Wölbung am Rand des Torus noch weiter aus,
    wodurch die Dichte der Zentren geringer wird.

*/

#include "kohonen_map.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

typedef ann::matrix matrix;
typedef ann::kohonen_map map;
typedef map::real_t real_t;
typedef map::size_t size_t;
typedef std::string string;

using std::exp;
using std::sin;
using std::cos;
using std::acos;
using std::atan2;
// @hint: Use atan2(y, x) to compute atan(y/x).

using std::sqrt;

using std::string;
using std::to_string;

using std::ofstream;
using std::cout;
using std::endl;


// Returns the square of the given value.
real_t square (real_t value_a)
{
  return value_a * value_a;
}

// Writes a Kohonen map to file (one line for each neuron's prototype).
void write_map (const std::string& filename_a, const map& map_a)
{
  // @task: Implement this.

    size_t R = map_a.R();
    size_t S = map_a.S();
    size_t T = map_a.mue_size();

    std::ofstream out_l(filename_a);
    for (size_t r = 0; r<R; r++)
    {
        for (size_t s =0; s< S; s++)
        {
            for (size_t t =0; t < T; t++)
            {
                out_l << map_a(r,s,t) << '\t';
            }
            out_l << std::endl;
        }
    }

}
// @hint: In gnuplot, splot can be used to plot 3-dimensional surface data.


matrix create_x()
{
    //create a x-vector
    matrix x(3,1);
    x.fill_with_uniform_samples(-2.0,2.0); //actually just for x(0,0) and x(1,0). x(2,0) needs to be overwritten

    matrix rand_m(1,1);
    rand_m.fill_with_uniform_samples(-0.1,0.1);
    real_t rand = rand_m(0,0);

    real_t r = 2.0/3.0;

    real_t dist = std::sqrt( square(x(0,0)) + square(x(1,0)) );
    if( (r < dist) && (dist < (3.0*r)) )
    {
        real_t tau = std::atan2( x(1,0),x(0,0) );
        real_t roh = std::acos( ( (x(0,0)/std::cos(tau) ) - 2.0 * r ) / r);

        x(2,0) = r * std::sin(roh) + rand;
    }
    else
        x(2,0) = rand;

    return x;
}


int main (int argc, char** argv)
{
  // @task: Create a 40x40 Kohonen map and initialize the prototypes according
  //        to task 6.1a.
    map kohonen_map(40,40,0.1);

  // @task: Train the map with samples of the function given on the sheet.
  //        Plot the map after the numbers of steps stated in 6.1f.

  // @hint: While testing/debugging your implementation you should be fine with
  //        10000 iterations to see where it's going.

    //console output config
    size_t count = 1;
    size_t j = 1;
    size_t step_size = 1000;

    //at these iteration counts we want to save the map to a file
    std::set<size_t> iteration_set = {1000,10000,100000,500000};
    size_t highest = *iteration_set.rbegin(); //this picks the highest value in the set

    write_map("kohonen_map_init.txt", kohonen_map);
    for(size_t i=1; i <= highest; i++)
    {
        matrix x = create_x(); // we need for every iteration a new x-vector
        kohonen_map.adapt_to( x(0,0), x(1,0), x(2,0) ); //training

        if(j == step_size) //console output
        {
            std::cout << std::endl << count;
            size_t zero_count = (size_t) std::log10(step_size);
            for(size_t k=0; k< zero_count; k++)
                std::cout << "0";
            std::cout <<" iterations...";

            count++; //counter for console output
            j=1; //reset counter
        }

        if( iteration_set.count(i) == 1) //save at this count of iterations
        {
            std::cout << "saved";
            write_map("kohonen_map_" + std::to_string(i) + ".txt", kohonen_map); //save to file
        }
        j++; //counter for console output
    }

  return 0;
}

